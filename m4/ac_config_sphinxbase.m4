dnl -*- Autoconf -*-

AC_DEFUN([AC_CONFIG_SPHINXBASE], [
  define([AC_SPHINXBASE_DEFAULT_PREFIX], [/usr/local])

  AC_CHECK_PROG([HAVE_PKGCONFIG], [pkg-config], [yes], [no])
  
  AS_IF([test x$HAVE_PKGCONFIG = xyes], [
    PKG_CHECK_MODULES([SPHINXBASE], [sphinxbase],
      [ac_sphinxbase_prefix=`pkg-config --variable=prefix sphinxbase`],
      [ac_sphinxbase_prefix=]AC_SPHINXBASE_DEFAULT_PREFIX)],
      [ac_sphinxbase_prefix=]AC_SPHINXBASE_DEFAULT_PREFIX)

  AC_MSG_CHECKING([location of CMU SphinxBase library])

  AC_ARG_WITH([sphinxbase],
    [AC_HELP_STRING([--with-sphinxbase],
      [CMU Sphinx libraries installdir default=${ac_sphinxbase_prefix}])],
    [case "$withval" in
       auto|yes) with_sphinxbase=${ac_sphinxbase_prefix} ;;
       no) AC_MSG_ERROR([bad value ${withval} for --with-sphinxbase]) ;;
       *) with_sphinxbase="$withval" ;;
     esac],
    [with_sphinxbase=${ac_sphinxbase_prefix}])

  AC_SUBST([SPHINXBASE_CPPFLAGS], ["\
    -I${ac_sphinxbase_prefix}/include \
    -I${ac_sphinxbase_prefix}/include/sphinxbase"])
    
  AC_SUBST([SPHINXBASE_LDFLAGS], [-L${ac_sphinxbase_prefix}/lib])
  AC_SUBST([SPHINXBASE_LIBS], [-lsphinxbase])

  AC_MSG_RESULT([${ac_sphinxbase_prefix}])
])# AC_WITH_SPHINX

